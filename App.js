
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import LoginScreen from './src/Screens/Login';
import DashboardScreen from './src/Screens/Dashboard';
import DialScreen from './src/Screens/Dial';
import AppointmentScreen from './src/Screens/Appoint';




const Stack = createNativeStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
        />
      <Stack.Screen 
      name = "Dashboard"
      component={DashboardScreen}
      options ={{title: 'Dashboard'}}
      />
      <Stack.Screen 
      name = "Dial"
      component={DialScreen}
      />
      <Stack.Screen 
      name = "Appoint"
      component={AppointmentScreen}
      />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MyStack;