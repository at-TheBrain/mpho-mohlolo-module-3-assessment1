import React from 'react';
import { StatusBar } from 'expo-status-bar';
import {Image,Alert, SafeAreaView, Button, TextInput ,StyleSheet, Text, View } from 'react-native';



const Separator = ()=> (
   <View style = {styles.separator} />
 );
 
 const App = ({navigation}) => (
   
   <><SafeAreaView style={styles.logo}>
       
   <Image
   style = {styles.imageLogo}
   source={require('C:/ReactNative/app_first/assets/try.png')}
 
   resizeMode = 'cover'
   ></Image>
  
   </SafeAreaView><SafeAreaView style={styles.container}>
 
 
       <View>
         <TextInput style={styles.textInput}
           placeholder=" Username" />
 
         <TextInput style={styles.textInput}
           placeholder="Password"
           secureTextEntry={true} />
       </View>
 
       <Separator />
 
       <View style={styles.position}
       >
         <Button
           title="Login"
           color={"black"}
           onPress={() => navigation.navigate('Dashboard')} />
     
         <Button
           title="SignUp"
           onPress={() => Alert.alert("\nGo Register at any hospital")}/>
       </View>
         <Separator/>
       <View style={styles.hos}>
           <Text
           style = {styles.smart}>Smart Hospital</Text>
       </View>
 
     </SafeAreaView></>
 
 
 );
 
 
 //CSS CODE
 const styles = StyleSheet.create({
     smart: {
         fontSize: 28,
         fontWeight: 'bold'
     },
     hos: {
        marginTop: 50,
      },
 imageLogo: {
  height: 370,
  width: 420,
  borderBottomLeftRadius: 75,
  borderBottomRightRadius: 75,
 
  
 },
 
 
 position: {
  flexDirection: 'row',
  borderRadius: 50,
 
  
 },
 
  separator: {
   borderBottomColor: 'black',
   borderBottomWidth: 0,
   
 
 },
 logo: {
   flex: 1,
   backgroundColor: "black",
   paddingVertical: 0,
   marginBottom: 200,
 
   //borderBottomRightRadius: 40,
   //borderBottomLeftRadius: 40,
  // borderButtomEndRadius: 50
   
 },
   
   container: {
    flex: 2,
    // backgroundColor: 'slateblue',
     alignItems: 'center',
    // justifyContent: 'center',
    borderTopEndRadius: 10,
   borderTopLeftRadius: 0
     
   },
   text:{
     fontSize: 17,
     color: 'coral',
     textAlign: 'left',
   },
   textInput:{
       borderColor: 'royalblue',
       borderWidth: 2,
     margin: 10,
     borderTopLeftRadius:250,
     borderTopRightRadius:250,
     borderBottomRightRadius: 250,
     width: 250,
     backgroundColor: 'white',
     padding: 9,   },}    
   
 );
 

export default App