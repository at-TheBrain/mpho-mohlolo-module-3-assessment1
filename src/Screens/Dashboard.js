
import React from 'react';
import {ScrollView,TouchableOpacity,Alert,Image, StyleSheet, Text} from 'react-native';



const Dashboard = ({navigation}) =>{
    return(
 <ScrollView style = {styles.container}>
     
     <TouchableOpacity 
      onPress={() => navigation.navigate('Dial')}>
<Image source={require('C:/ReactNative/app_first/assets/callAmbulance.jpg')}
       style = {styles.DialContainer}>
             </Image>
             <Text style = {styles.smart}>       Call an Ambulance</Text>
    </TouchableOpacity>




     <TouchableOpacity 
      onPress={() => navigation.navigate('Appoint')}>
<Image source={require('C:/ReactNative/app_first/assets/docAppoint.jpg')}
       style = {styles.app}>
             </Image><Text style = {styles.smart}>Book for an Appointment</Text>
    </TouchableOpacity>

    <TouchableOpacity 
      onPress={() => Alert.alert('Searching. Please wait...')}>
<Image source={require('C:/ReactNative/app_first/assets/location.jpg')}
       style = {styles.orange}>
             </Image>
             <Text style = {styles.smart}>Locate nearest Hospitals</Text>
    </TouchableOpacity>
     
    
 </ScrollView>
    );
} 


//CSS
const styles = StyleSheet.create({
    smart: {
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: 99
    },
    orange:{
        height: 230,
         marginLeft: 31,
        marginTop: 50,
        width: 350,
        borderColor: 'orange',
        borderWidth: 4,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,

    },
    app:{
        height: 230,
         marginLeft: 31,
        marginTop: 50,
        width: 350,
        borderColor: 'blue',
        borderWidth: 4,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,

    },

container:{
    flex: 1 , 
     backgroundColor: 'white'
    

},

 DialContainer:{
     height: 230,
    marginLeft: 31,
    marginTop: 50,
    width: 350,
    borderColor: 'red',
    borderWidth: 4,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
 

    

 },

appointContainer:{
    flex: 1,
    marginTop: 25,
    marginBottom: 25,
    marginLeft: 15,
    marginRight: 15,
    
    backgroundColor:'black',
 borderBottomRightRadius: 90,
 borderBottomLeftRadius: 90,
 borderTopRightRadius: 90,
 borderTopLeftRadius: 90,
   
    backgroundColor:'black'

},


});

export default Dashboard